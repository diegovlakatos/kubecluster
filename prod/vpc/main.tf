module "vpc" {
  source       = "../../modules/vpc"
  zone_name    = var.zone_name
  subnet_count = var.subnet_count
  vpc_name     = var.vpc_name
  cidr_block   = var.cidr_block
}

output "vpc_id" {
  value = module.vpc.vpc_id
}
output "private_subnets" {
  value = module.vpc.private_subnets
}

output "public_subnets" {
  value = module.vpc.public_subnets
}