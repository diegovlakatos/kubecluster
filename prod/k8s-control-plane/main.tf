data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

#module "keypair" {
#  source = "../../modules/keypair"
#  env_name = var.env_name
#  generate_ssh_key = var.generate_ssh_key
#}

resource "aws_iam_instance_profile" "test_profile" {
  name = "k8s-control-plane-instance-profile"
  role = aws_iam_role.k8s-control-plane-role.name
}

resource "aws_iam_role" "k8s-control-plane-role" {
  name = "k8s-control-plane-role"
  path = "/"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
  managed_policy_arns = [aws_iam_policy.k8s-control-plane-policy.arn]
}

resource "aws_iam_policy" "k8s-control-plane-policy" {
  name = "k8s-control-plane-policy"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "VisualEditor0",
        "Effect" : "Allow",
        "Action" : [
          "ssm:DescribeDocument",
          "ec2messages:GetEndpoint",
          "ec2messages:GetMessages",
          "ssmmessages:OpenControlChannel",
          "ssm:PutConfigurePackageResult",
          "ssm:ListInstanceAssociations",
          "ssm:GetParameter",
          "ssm:UpdateAssociationStatus",
          "ssm:GetManifest",
          "ec2messages:DeleteMessage",
          "ssm:UpdateInstanceInformation",
          "ssm:DescribeParameters",
          "ec2messages:FailMessage",
          "ssmmessages:OpenDataChannel",
          "ssm:GetDocument",
          "ssm:PutComplianceItems",
          "ssm:DescribeAssociation",
          "ssm:GetDeployablePatchSnapshotForInstance",
          "ec2messages:AcknowledgeMessage",
          "ssm:GetParameters",
          "ssmmessages:CreateControlChannel",
          "ssm:PutParameter",
          "ssmmessages:CreateDataChannel",
          "ssm:PutInventory",
          "ec2messages:SendReply",
          "ssm:ListAssociations",
          "ssm:UpdateInstanceAssociationStatus",
          "s3:*"
        ],
        "Resource" : "*"
      }
    ]
  })
}

resource "aws_launch_configuration" "k8s-control-plane" {
  name                 = "kubernetes-control-plane3"
  image_id             = data.aws_ami.ubuntu.id
  instance_type        = "t3.medium"
  iam_instance_profile = aws_iam_instance_profile.test_profile.name
  security_groups      = [aws_security_group.control-plane.id]
  root_block_device {
    volume_type = "gp2"
    volume_size = "20"
    encrypted   = "true"
  }
  lifecycle {
    create_before_destroy = true
  }
  user_data = file("user-data-control-plane.sh")
}

resource "aws_lb" "control-plane-lb" {
  name               = "control-plane-LB"
  internal           = false
  load_balancer_type = "network"
  subnets            = split(",", var.public_subnets)

}

resource "aws_lb_target_group" "control-plane" {
  name     = "contro-plane"
  port     = 6443
  protocol = "TCP"
  vpc_id   = var.vpc_id
}

resource "aws_lb_listener" "control-plane" {
  load_balancer_arn = aws_lb.control-plane-lb.arn
  port              = "6443"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.control-plane.arn
  }
}

resource "aws_ssm_parameter" "join_command" {
  name        = "/production/k8s/join_command_worker"
  description = "The parameter description"
  type        = "SecureString"
  value       = "NOT_INITIALIZED"

  tags = {
    environment = "production"
  }
}

resource "aws_ssm_parameter" "join_command_control_plane" {
  name        = "/production/k8s/join_command_control_plane"
  description = "The parameter description"
  type        = "SecureString"
  value       = "NOT_INITIALIZED"

  tags = {
    environment = "production"
  }
}

resource "aws_s3_bucket" "k8s-control-plane-files" {
  bucket = "lakatos-control-plane"
  acl    = "private"
}

resource "aws_security_group" "control-plane" {
  name        = "control-plane-sg"
  description = "control-plane-sg"
  vpc_id      = var.vpc_id

  ingress {
    description = "anywhere"
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}


module "asg" {
  source              = "../../modules/asg"
  asgname             = var.asgname
  launchconfiguration = aws_launch_configuration.k8s-control-plane.name
  asgminsize          = var.asgminsize
  asgmaxsize          = var.asgmaxsize
  subnets             = [var.private_subnets]
  target_group_arns   = aws_lb_target_group.control-plane.arn
}
