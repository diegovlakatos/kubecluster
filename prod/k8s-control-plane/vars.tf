variable "asgname" {
  default = "k8s-control-plane"
}
variable "launchconfiguration" {
  default = "launchconfig"
}

variable "asgminsize" {
  default = 3
}

variable "asgmaxsize" {
  default = 3
}
variable "private_subnets" {
  default = "subnet-123"
}

variable "public_subnets" {
  default = "subnet-321"
}

variable "vpc_id" {
  default = "vpc"
}

variable "security_groups" {
  default = "SG"
}
