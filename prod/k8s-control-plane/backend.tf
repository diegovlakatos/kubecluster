# Generated by Terragrunt. Sig: nIlQXj57tbuaRZEa
terraform {
  backend "s3" {
    bucket = "terraform-lakatos-state-3"
    key    = "k8s-control-plane/terraform.tfstate"
    region = "us-east-1"
  }
}
