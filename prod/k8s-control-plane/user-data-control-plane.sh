#!/bin/bash

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat << EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

sudo apt-get update
sudo apt-get install -y docker-ce=5:19.03.15~3-0~ubuntu-focal kubelet=1.20.5-00 kubeadm=1.20.5-00 kubectl=1.20.5-00 awscli jq
sudo apt-mark hold docker-ce kubelet kubeadm kubectl

sudo mkdir /etc/docker
cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

systemctl restart docker

export AZ=$(curl http://169.254.169.254/latest/meta-data/placement/availability-zone | cut -f3 -d"-")
export JOIN_COMMAND=$(aws ssm get-parameter --name /production/k8s/join_command_control_plane --region $(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region) --with-decryption | jq -r .Parameter.Value)
export CLUSTER_INITIALIZED=False

if [ $JOIN_COMMAND = NOT_INITIALIZED ]
then
    if [ $AZ = "1a" ]
    then
        sudo kubeadm init --pod-network-cidr=10.244.0.0/16 --control-plane-endpoint control-plane-lb-276ce3951fa25da0.elb.us-east-1.amazonaws.com:6443 --upload-certs >> kubeadm_output.txt
        sudo -u ubuntu mkdir -p /home/ubuntu/.kube
        sudo cp -i /etc/kubernetes/admin.conf /home/ubuntu/.kube/config
        sudo chown ubuntu:ubuntu /home/ubuntu/.kube/config
        aws s3 cp /home/ubuntu.kube/config s3://lakatos-control-plane/kubeconfig
        sudo -u ubuntu kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml
        aws ssm put-parameter --name /production/k8s/join_command_worker --value "$(grep -A2 "kubeadm join" kubeadm_output.txt | tr -d '\n' | tr -d '\\')" --region us-east-1 --type SecureString --overwrite
    else
        while [ $JOIN_COMMAND = NOT_INITIALIZED ]; do
            export JOIN_COMMAND=$(aws ssm get-parameter --name /production/k8s/join_command --region $(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region) --with-decryption | jq -r .Parameter.Value)
            sleep 30
        done
        aws ssm get-parameter --name /production/k8s/join_command --region $(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region) --with-decryption | jq -r .Parameter.Value
        sudo -u ubuntu mkdir -p /home/ubuntu/.kube
        sudo cp -i /etc/kubernetes/admin.conf /home/ubuntu/.kube/config
        sudo chown ubuntu:ubuntu /home/ubuntu/.kube/config
    fi
fi
