# Kubecluster

## Not finished!!

The goal of this code is to use terraform to build a high-available kubernetes cluster.
It uses terragrunt as a wrapper/orchestration tool for the terraform modules

To run this code you need:
- aws cli
- terraform
- terragrunt

To run the code navigate to the "prod" folder and execute ```terragrunt run-all apply```

# To-do

- Create the worker nodes section
- Finish the script that is reponsible for bootstraping the environment
- refactor code to remove hard-coded configurations and improve the code re-utilization.
- improve security of the solution (at the moment is intentionally configured to allow all connections in order to facilitate the integration with Giltab)
- Develop a pipeline to automatically execute the steps when changes are made